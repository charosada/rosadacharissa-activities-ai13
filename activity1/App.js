import React, { useState } from 'react';
import { StyleSheet, View, Button, TextInput, Alert } from 'react-native';

export default function App() {
  const [entertext, setText] = useState('');
  const getValues = () => {

    Alert.alert('You just typed:', entertext);
  };
  return (
    <View style={styles.container}>
      <TextInput
        style={styles.input}
        onChangeText={(textinput) => setText(textinput)}
        placeholder="Enter text here"/>
      <Button onPress={() => getValues()} 
      title="Go" 
      color="#000000" />
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: {
    width: 250,
    height: 50,
    margin: 20,
    borderColor: 'black',
    borderWidth: 2,
    padding: 10,
  },
});

import React, { useState } from 'react';

import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  Image,
  Button,
} from 'react-native';


import AppIntroSlider from 'react-native-app-intro-slider';

const App = () => {
  const [showRealApp, setShowRealApp] = useState(false);

  const onDone = () => {
    setShowRealApp(true);
  };

  const RenderItem = ({ item }) => {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: item.backgroundColor,
          alignItems: 'center',
          justifyContent: 'space-around',
          paddingBottom: 80,
        }}>
        <Text style={styles.introTitleStyle}>{item.title}</Text>
        <Image style={styles.introImageStyle} source={item.image} />
        <Text style={styles.introTextStyle}>{item.text}</Text>
      </View>
    );
  };

  return (
    <>
      {showRealApp ? (
        <SafeAreaView style={styles.container}>
          <View style={styles.container}>
            <Text style={styles.titleStyle}>
            </Text>
            <Text style={styles.paragraphStyle}>
            </Text>
            <Button
              title="Back to Home"
              onPress={() => setShowRealApp(false)}
            />
          </View>
        </SafeAreaView>
      ) : (
        <AppIntroSlider
          data={slides}
          renderItem={RenderItem}
          onDone={onDone}
          
        />
      )}
    </>
  );
};

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#d5d7db',
    alignItems: 'center',
    padding: 1,
    justifyContent: 'center',
  },
  titleStyle: {
    padding: 1,
    textAlign: 'center',
    fontSize: 1,
    fontWeight: 'bold',
  },
  paragraphStyle: {
    padding: 20,
    textAlign: 'center',
    fontSize: 16,
  },
  introImageStyle: {
    width: 400,
    height: 400,
  },
  introTextStyle: {
    fontSize: 30,
    color: 'black',
    textAlign: 'center',
    paddingVertical: 5,
  },
  introTitleStyle: {
    fontSize: 20,
    color: 'black',
    textAlign: 'center',
    marginBottom: 6,
    fontWeight: 'bold',
  },
});

const slides = [
  {
    key: 'home',
    text: 'Why I chose IT course?',
    title: 'HOME',
    image: require('./images/home.png'),
    backgroundColor: '#d5d7db',
   },
  {
    key: 'screen1',
    title: 'PERSONAL REASON',
    text: 'I want to learn and explore more about multimedia like editing and designing.I want also to share my knowledge in the future generations. ',
    image: require('./images/screen1.png'),
    backgroundColor: '#d5d7db',
  },
  {
    key: 'screen2',
    title: 'AS A JOB',
    text: 'IT is known as an on-demand job around the world. It has a lot of offers and I want to have a better job with a good salary.',
    image: require('./images/screen2.png'),
    backgroundColor: '#d5d7db',
   },
  {
    key: 'screen3',
    title: 'TECHNOLOGY IS LIFE',
    text: 'Technology makes our life easier, it plays a significant role in our lives.',
    image: require('./images/screen3.png'),
    backgroundColor: '#d5d7db',
  },
  {
    key: 'screen4',
    title: 'FOR THE SOCIETY',
    text: 'As a member of society, I want to become an instrument that has appropriate knowledge about technology that may help in achieving a better future. ',
    image: require('./images/screen4.png'),
    backgroundColor: '#d5d7db',
  },
];

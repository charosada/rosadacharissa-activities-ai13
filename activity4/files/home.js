import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer } from "react-navigation";
import HomeScreen from "../screens/HomeScreen"; 
import Screen1 from "../screens/Screen1";

const screens = {
    Home:{
        screen:HomeScreen
    },
    Reason1:{
        screen:Screen1
    }
}

const home = createStackNavigator(screens);

export default createAppContainer(home);
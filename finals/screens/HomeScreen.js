import React, { useState } from "react";
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  ScrollView,
  Image,
} from "react-native";
import { Categories } from "../components/list";
import DetailsScreen from "./DetailsScreen";

const HomeScreen = ({ navigation }) => {
  const [currentSelected, setcurrentSelected] = useState([0]);
  const renderCategories = ({ item, index }) => {
    return (
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={() => setcurrentSelected(index)}
      >
        <View
          style={{
            width: 115,
            height: 40,
            borderRadius: 30,
            justifyContent: "space-evenly",
            alignContent: "center",
            backgroundColor: currentSelected == index ? "#ffc300" : "white",
            elevation: 5,
            margin: 5,
          }}
        >
          <Text
            style={{
              fontSize: 18,
              color: "black",
              fontWeight: "700",
              textAlign: "center",
            }}
          >
            {" "}
            {item.name}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  const renderItems = (data, index) => {
    return (
      <TouchableOpacity
        key={index}
        activeOpacity={0.8}
        style={{
          width: "100%",
          height: 150,
          alignItems:"center",
          justifyContent: "center",
        }}
        onPress={()=> navigation.push('DetailsScreen',{
         name:data.name,
        description:data.description,
         image:data.image,
         ingredients:data.ingredients,
     
        })}
      >
        <View
          style={{
            width: "100%",
            height: 130,
            paddingTop: 30,
            backgroundColor: "#ffc300",
            borderRadius: 30,
            elevation: 5,
            alignItems: "center"
          }}
        >
          <Text
          style={{
            fontSize: 25,
            fontWeight: '600',
            paddingTop: 10,
            marginLeft: 1,
        
            
          }}
          >
            {data.name}
          </Text>
          <Text>{data.servings}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView
      style={{
        flex: 1,
        paddingHorizontal: 20,
        backgroundColor: "white",
      }}
    >
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={style.header}>
          <View>
            <Text
              style={{
                fontSize: 30,
                fontWeight: "bold",
              }}
            >
             
            </Text>
            <Text
              style={{
                fontSize: 45,
                fontWeight: "bold",
                color: "black",
              }}
            >
             Best Filipino Ulam Recipes
            </Text>
            <Text
              style={{
                paddingTop: 10,
              }}
            ></Text>
            <FlatList
              horizontal={true}
              data={Categories}
              renderItem={renderCategories}
              showsHorizontalScrollIndicator={false}
            />
            <Text
              style={{
                paddingTop: 20,
                fontSize: 16,
                fontWeight: "600",
                paddingHorizontal: 10,
              }}
            >
              Available Dishes
            </Text>

            {Categories[currentSelected].items.map(renderItems)}
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};
const style = StyleSheet.create({
  header: {
    marginTop: 30,
    flexDirection: "row",
    justifyContent: "space-between",
  },
});

export default HomeScreen;

import React from "react";
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
} from "react-native";
import FontAwesome from "react-native-vector-icons/FontAwesome";

const DetailsScreen = ({ navigation, route }) => {
  const { name, servings, image, ingredients, description } = route.params;
  return (
    <View
      style={{
        width: "100%",
        height: "100%",
        backgroundColor: "white",
        position: "relative",
      }}
    >
      <View
        style={{
          padding: 20,
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={{
            width: 50,
            height: 40,
          }}
        >
          <FontAwesome
            name="arrow-left"
            style={{
              fontSize: 30,
              color: "black",
            }}
          />
          <Text>Back</Text>
        </TouchableOpacity>
      </View>
      <Text
        style={{
          fontSize: 40,
          color: "black",
          fontWeight: "800",
          paddingHorizontal: 10,
          maxWidth: 310,
        }}
      >
        {name}
      </Text>

      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          paddingHorizontal: 20,
        }}
      >
        <Text
          style={{
            fontSize: 15,
            color: "black",
            fontWeight: "500",
          }}
        >
          {description}
          {ingredients}
        </Text>
       
      </View>
      <View
        style={{
          flexDirection: "row",
          maxHeight: 500,
          width: "100%",
          alignItems: "center",
        }}
      >
        <View
          style={{
            width: 380,
            height: 500,
          }}
        >
          <Image
            source={image}
            style={{
              width: "100%",
              height: "100%",
              resizeMode: "contain",
            }}
          />
        </View>
      </View>
    </View>
  );
};

export default DetailsScreen;

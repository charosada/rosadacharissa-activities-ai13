import * as React from "react";
import {
  Text,
  View,
  ImageBackground,
  StatusBar,
  TouchableOpacity,
} from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import {NavigationContainer} from '@react-navigation/native'



const SplashScreen = ({ navigation }) => {
  function renderTitle() {
    return (
      <View
        style={{
          
          height: "75%",
        }}
      >
        <ImageBackground
          source={{
            uri: "https://images.unsplash.com/photo-1536489885071-87983c3e2859?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=987&q=80",
          }}
          style={{
            flex: 1,
            justifyContent: "flex-end",
          }}
          resizeMode="cover"
        >
          <LinearGradient
            start={{
              x: 0,
              y: 0,
            }}
            end={{
              x: 0,
              y: 1,
            }}
            colors={["transparent", "black"]}
            style={{
              height: 260,
              justifyContent: "flex-end",
            }}
          >
            <Text
              style={{
                width: "100%",
                fontSize: 38,
                fontWeight: "bold",
                color: "#ffffff",
                paddingHorizontal: 25,
                paddingVertical: 15,
                textAlign: 'center'
              }}
            >
            Best Filipino Dishes Recipe Guide
            </Text>
          </LinearGradient>
        </ImageBackground>
      </View>
    );
  }

  function renderDescription() {
    return (
      <View
        style={{
          flex: 1,
        }}
      >
        <Text
          style={{
            color: "gray",
            paddingHorizontal: 35,
            width: "100%",
            fontSize: 14,
            textAlign: "center",
          }}
        >
          The smartphone will now be converted into a cookbook. An application that provides recipes of the famous Filipino dishes. Descriptions and recipe list Kawaling Pinoy.

        </Text>
        <View
          style={{
            flex: 1,
            justifyContent: "center",
          }}
        ></View>
      </View>
    );
  }

  function renderButton() {
    return (
      <View
        style={{
          flex: 1,
       alignItems: 'center'
   
        }}
      >
        <TouchableOpacity 
       activeOpacity={0.8} 
       onPress={() => navigation.navigate("HomeScreen")}
          style={{
            backgroundColor: "#ffc300",
            padding: 12,
            width: "50%",
            borderRadius: 30,
            flexDirection: "row",
            justifyContent: "center",
       
          }}
        >
          <Text
          style={{
            color: "black",
            fontSize: 20,
            fontWeight: 'bold',
            textAlign: "center",
          }}
          
          >
            Get Started
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: "#000000",
      }}
    >
      <StatusBar barStyle="light-content" />
      {renderTitle()}

      {renderDescription()}

      {renderButton()}
    </View>
  );
};

export default SplashScreen;

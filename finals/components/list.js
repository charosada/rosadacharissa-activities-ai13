export const Categories = [
  {
    name: "Pork",
    items: [
      {
        name: "Pork Adobo",
        image: require("../components/images/pork/porkadobo.jpg"),
        servings: "4 servings",
        description:
          "Filipino Adobo made with pork belly is a delicious medley of salty and savory you'll love with steamed rice. It's easy to make, hearty, tasty, and sure to be a dinner favorite!",
        ingredients:
          "\n\nIngredients: \n- 2 pounds pork belly, cut into - 2-inch cubes \n - 1 onion peeled and sliced thinly \n - 1 head garlic, peeled and minced \n  - 2 bay leaves \n - 1 teaspoon salt \n - 1/2 teaspoon pepper \n -1 tablespoon oil \n - 1 cup vinegar \n - 1/2 cup soy sauce \n - 1 cup water",
      },
      {
        name: "Lechon Kawali",
        image: require("../components/images/pork/porklechonkawali.jpg"),
        servings: "8 servings",
        description:
          "Crispy Pork Belly simmered to tenderness with aromatics and dee-fried to golden perfection. Crunchy on the outside and super moist on the inside, it's the ultimate pork dish!",
        ingredients:
          "\n\n Ingredients: \n - 4 pounds whole pork belly\n - 1 head garlic, pounded \n - 2 tablespoons salt \n - 1 tablespoon peppercorns \n - 2 bay leaves \n - water \n - canola oil",
      },
      {
        name: "Pork Sinigang",
        image: require("../components/images/pork/porksinigang.jpg"),
        servings: "4 servings",
        description:
          "Pork Sinigang is a delightful sour soup made with pork ribs, vegetables, and tamarind-flavored broth. It's hearty, tasty and perfect for cold weather.",
        ingredients:
          "\n\nIngredients: \n - 2 pounds pork spare ribs, cut into 2-inch pieces \n - 8 cups water \n- 2 large tomatoes, quartered \n- 1 medium onion, peeled and quartered \n- 2 tablespoons fish sauce \n- 6 pieces gabi, (peeled and halved depending on size) \n- 1 6-inch radish (labanos), peeled and sliced to 1/2-inch thick half-rounds \n- 2 finger chilies (siling haba) \n - 1/2 bunch long beans (sitaw), ends trimmed and cut into 3-inch lengths\n - 1 eggplant, ends trimmed and sliced to 1/2-inch thick half-rounds\n - 6 pieces okra, ends trimmed\n - 15 pieces large tamarind or 1 1/2 (1.41 ounces each) packages tamarind base powder \n - salt and pepper to taste \n- 1 bunch bok choy or pechay, ends trimmed and separated into leaves",
      },
      {
        name: "Pork Menudo",
        image: require("../components/images/pork/porkmenudo.jpg"),
        servings: "6 servings",
        description:
          "Filipino Menudo is a hearty and tasty pork stew made with fresh tomatoes, potatoes, carrots, and garbanzo beans. It's easy to make and delicious with steamed rice!",
        ingredients:
          "\n\nIngredients: \n-	2 tablespoons canola oil \n - 1 teaspoon annatto seeds \n - 1 onion, peeled and chopped \n - 2 cloves garlic, peeled and minced \n - 2 pounds boneless pork shoulder, cut into 1-inch cubes \n - 6 large tomatoes, chopped \n - 1 tablespoon fish sauce \n - 2 cups water \n - 3 medium potato, peeled and cut into 1-inch cubes \n - 2 large carrots, peeled and cut into 1-inch cubes \n - 1/2 pound beef liver, cut into 1-inch cubes \n - 1 can (16 ounces) garbanzo beans, drained well \n - 1/2 green bell pepper, cored, seeded and cut into 1-inch cubes \n - 1/2 red bell pepper, cored, seeded and cut into 1-inch cubes \n - 1/4 cup raisins \n - salt and pepper to taste",
      },
    ],
  },
  {
    name: "Chicken",
    items: [
      {
        name: "Chicken Adobo",
        image: require("../components/images/chicken/chickenadobo.jpg"),
        servings: "4 servings",
        description:
          "Chicken Adobo made extra special with pan-fried potatoes. Hearty and delicious with a garlicky savory sauce, this classic Filipino stew is perfect with steamed rice.",
        ingredients:
          "\n\nIngredients: \n-	1 (3 to 4 pounds) whole chicken, cut into serving parts \n - 1 onion, peeled and sliced thinly \n - 1 head garlic, peeled and minced \n - 2 bay leaves \n - 1/4 cup soy sauce \n - 1/2 teaspoon peppercorns \n - 1/4 cup canola oil \n - 2 large potatoes, peeled and sliced into rounds \n - 1/2 cup vinegar \n - 1 cup water\n - salt and pepper to taste",
      },
      {
        name: "Chicken Curry",
        image: require("../components/images/chicken/chickencurry.jpg"),
        servings: "6 servings",
        description:
          "Filipino-style coconut chicken curry is hearty, tasty, and sure to be a family favorite Easy to make with only a handful of ingredients, cooks in one pan, and is ready in an hour. Perfect for busy weeknights!",
        ingredients:
          "\n\nIngredients: \n-	1/4 cup canola oil \n - 2 medium potatoes, peeled and quartered \n - 2 large carrots, peeled and cubed\n - 1/2 green bell pepper, cored, seeded and cut into cubes\n - 1/2 red bell pepper, cored, seeded and cut into cubes\n - 1 onion, peeled and cubed\n - 3 cloves garlic, peeled and minced\n - 1 thumb-size ginger, peeled and julienned\n - 1 (3 pounds) bone-in chicken, cut into serving pieces\n - 1 tablespoon fish sauce\n - 1 cup coconut milk\n - 1 cup water\n - 2 tablespoons curry powder\n - salt and pepper to taste",
      },
      {
        name: "Chicken Tinola",
        image: require("../components/images/chicken/chickentinola.jpg"),
        servings: "6 servings",
        description:
          "Chicken tinola is a healthy, tasty, and comforting soup that's perfect for cold weather. It's delicious on its own or with steamed rice.",
        ingredients:
          "\n\nIngredients: \n-	1 tablespoon canola oil\n - 1 small onion, peeled and sliced thinly\n - 3 cloves garlic, peeled and minced\n - 2 thumb-sized fresh ginger\n - 1 (3 to 4 pounds) whole chicken, cut into serving pieces\n - 2 tablespoons fish sauce\n - 5 cups water \n - 1 small green papaya, pared, seeded and cut into 2-inch wedges \n - 1 bunch fresh spinach leaves, stems trimmed \n - salt and pepper to taste",
      },
      {
        name: "Chicken Inasal",
        image: require("../components/images/chicken/chickeninasal.jpg"),
        servings: "8 servings",
        description:
          "Chicken Inasal is a Filipino-style BBQ chicken flavored with vinegar, citrus juice, lemongrass, and achiote, and grilled to perfection. It's juicy, flavorful, and delicious served with steamed rice.",
        ingredients:
          "\n\nIngredients: \n-2 stalks lemongrass \n - 1 head garlic, peeled and minced\n - 1 thumb-size ginger, peeled and grated\n - 1/2 cup palm vinegar \n - 1/4 cup calamansi juice\n - 1/4 cup brown sugar \n - 1 1/2 teaspoons salt\n - 1/2 teaspoon pepper\n - 2 (about 3 to 4 pounds each) whole chicken, quartered",
      },
    ],
  },
  {
    name: "Vegetables",
    items: [
      {
        name: "Vegetable Lumpia",
        image: require("../components/images/vegetables/lumpia.jpg"),
        servings: "12 servings",
        description:
          "Lumpiang Gulay are tasty snack or appetizer the whole family will love. Filled with tofu and vegetables, these crispy spring rolls are nutritious as they are delicious!",
        ingredients:
          "\n\nIngredients: \n - 1/4 cup canola oil\n - 1 package (12 ounes) firm tofu\n - 1 onion, peeled and chopped\n - 2 cloves garlic, peeled and minced\n - 1 large Japanese yam (camote), peeled and cut into 1-inch lengths matchsticks\n - 1/2 cup water\n - 2 large carrots, peeled and cut into 1-inch lengths matchsticks\n - 1/2 head cabbage, shredded\n - 4 cups bean sprouts togi \n -12 spring roll wrappers\n - salt and pepper to taste",
      },
      {
        name: "Pinakbet",
        image: require("../components/images/vegetables/pinakbet.jpg"),
        servings: "4 servings",
        description:
          "Pakbet is a Filipino dish made of pork and vegetables such as eggplant, okra, bitter melon, long beans, and calabasa. It's a delicious and nutritious dish served as a main entree or a side to fried fish or grilled meat.",
        ingredients:
          "\n\nIngredients: \n-	1 tablespoon canola oil\n - 1 onion, peeled and chopped\n - 2 cloves garlic, peeled and minced\n - 1/2 pound pork belly, cut into 1-inch cubes\n - 1 tablespoon shrimp paste\n - 2 Roma tomatoes, chopped\n - 2 cups water\n - 1/2 small kalabasa, peeled and cut into pieces\n - 8 okra, ends trimmed\n - 1/2 bunch long beans, ends trimmed and cut into 3-inch lengths\n - 1 medium ampalaya (bittermelon), seeded, halved and cut into 1-inch thick \n - 1 large eggplant, ends trimmed and cut into 1-inch thicksalt and pepper to taste ",
      },
      {
        name: "Laing",
        image: require("../components/images/vegetables/laing.jpg"),
        servings: "6 servings",
        description:
          "Laing made of dried taro leaves cooked with pork belly, coconut milk, and chili peppers is easy to make and sure to deliver big flavors. This classic Bicolano dish is delicious with steamed rice!",
        ingredients:
          "\n\nIngredients: \n- 2 ounces dried gabi leaves\n - 2 lemongrass stalks\n - 1 tablespoon canola oil\n - 1 onion, peeled and chopped\n - 4 cloves garlic, peeled and minced\n - 1 thumb-size ginger, peeled and minced\n - 1 pound pork belly, diced pepper to taste\n - 1 tablespoon shrimp paste\n - 4 cups coconut milk\n - 8 Thai chili peppers, chopped\n -  1cup coconut cream\n - salt to taste",
      },
      {
        name: "Adobong Sitaw",
        image: require("../components/images/vegetables/sitaw.jpg"),
        servings: "4 servings",
        description:
          "Adobong Sitaw with Pork made of long beans and pork belly is an easy weeknight dinner that packs great flavors! This adobo-style vegetable dish is hearty, tasty, and pairs well with steamed rice.",
        ingredients:
          "\n\nIngredients: \n - 1 tablespoon canola oil \n - 1 pound pork belly, cut into thin strips\n - 1 onion, peeled and sliced thinly\n - 5 cloves garlic, peeled and minced\n - 3/4 cup vinegar\n - 1/4 cup soy sauce\n - 1/2 cup water\n - 1 bunch yard long beans (sitaw), ends trimmed and cut into 3-inch lengths\n - salt and pepper to taste\n - fried garlic bits, optional ",
      },
    ],
  },
];

import * as React from 'react';
import { Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import TaskScreen from './screens/TaskScreen';
import HomeScreen from './screens/Activity2';
import Ionicons from 'react-native-vector-icons/Ionicons';
import ProfileScreen from './screens/ProfileScreen';

const Tab = createBottomTabNavigator();

export default function App() {
  return (
    
    <NavigationContainer>
      <Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;

            if (route.name === 'ToDoList') {
              iconName = focused
                ? 'list'
                : 'list';
            } else if (route.name === 'Completed Tasks') {
              iconName = focused 
              ? 'folder' 
              : 'folder';
            }
            else if (route.name === 'Profile') {
              iconName = focused 
              ? 'person' 
              : 'person';
            }

            return <Ionicons name={iconName} size={size} color={color} />;
          },
          tabBarActiveTintColor: 'red',
          tabBarInactiveTintColor: 'black',
        })}
      >
        <Tab.Screen name="ToDoList" component={HomeScreen} />
        <Tab.Screen name="Completed Tasks" component={TaskScreen} />
        <Tab.Screen name="Profile" component={ProfileScreen} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}

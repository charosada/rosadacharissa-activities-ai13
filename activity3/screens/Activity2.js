import React, { useState } from "react";
import {StyleSheet,Text,  View,TextInput,TouchableOpacity, Keyboard, ScrollView,} 
from "react-native";
import Task from "./components/Task";

export default function App() {
  const [task, writetask] = useState();
  const [taskItems, taskitems] = useState([]);

  const addtask = () => {
    Keyboard.dismiss();
    taskitems([...taskItems, task]);
    writetask("");
  };

  const completeTask = (index) => {
    let itemsCopy = [...taskItems];
    itemsCopy.splice(index, 1);
    taskitems(itemsCopy);
  };
  
  return (
    
    <View style={styles.container}>
      <Text style={styles.title}>To-Do List</Text>
     <TextInput style={styles.textinput} placeholder={'Write a task'} value={task} onChangeText={text => writetask(text)} />
      <View style={styles.addbutton}>

        <TouchableOpacity onPress={() =>addtask()}>
          <Text style={styles.add}>Add</Text>
        </TouchableOpacity>
      </View>
      <ScrollView
        style={styles.scroll}
        contentContainerStyle={{ flexGrow: 1 }}
        keyboardShouldPersistTaps="handled"
      >
        <View style={styles.items}>
          {taskItems.map((item, index) => {
            return (
              <TouchableOpacity key={index} onPress={() => completeTask(index)}>
                <Task text={item} />
              </TouchableOpacity>
            );
          })}
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f2f2',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    marginTop: 60,
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: "center",
    color: '#788eec',
  },
  scroll: {
    marginTop: 20,
    width: "90%",
  },
  textinput: {
    marginTop: 20,
    paddingVertical: 15,
    paddingHorizontal: 10,
    backgroundColor: '#ffffff',
    borderColor: "#23272b",
    borderWidth: .8,
    borderRadius: 5,
    width: "90%",
  },
  addbutton: {
    marginTop: 15,
    paddingVertical: 10,
    backgroundColor: '#788eec',
    borderColor: "#000000",
    borderRadius: 3,
    width: "90%", 
  },
  add: {
    paddingHorizontal: 0,
    textAlign: "center",
    color: '#ffffff',
    fontWeight: "bold",
  },
});

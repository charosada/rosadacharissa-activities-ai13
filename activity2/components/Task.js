import React from "react";
import { View, Text, StyleSheet } from "react-native";

const Task = (design) => {
    return (
        <View style={styles.itemtask}>
            <View style={styles.itemLeft}>
                <View style={styles.circle}></View>
                <Text style={styles.itemtext}>{design.text}</Text>
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    itemtask: {
        backgroundColor: '#ffffff',
        padding: 20,
        borderRadius: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10,
        borderColor: 'grey',
        borderWidth: 0.2, 
    },
    itemLeft: {
        flexDirection: 'row',
        alignItems: 'center',
        flexWrap: 'wrap',
    },
    circle: {
        width: 15,
        height: 15,
        borderColor: '#788eec',
        borderWidth: 2,
        borderRadius: 5,
        marginRight: 15,
    },
    itemtext: {
        maxWidth: '85%',
        color: '#000000',
        fontSize: 16,
    },
});
export default Task;